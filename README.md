# COP Research at a Glance
Michael L. Bernauer  
01/29/2015  
_by_ [_Michael L. Bernauer_](http://mlbernauer@bitbucket.org/)

Researchers at the University of New Mexico College of Pharmacy are constantly
conducting research on a variety to topics. Although much of this research gets published
in peer reviewed journals that require membership, most of the abstracts can be accessed
in PubMed. Access to this information allows us to get a general idea of the research published
at the College of Pharmacy.

In this document we will explore research themes at the college by analyzing abstracts that have
been indexed in PubMed. We will start by downloading all available abstracts. We will then generate
a corpus and apply some transofrmations to clean up the data. Finally we will construct a wordcloud
to get a general understanding of the terms frequently used by faculty to describe their researchdd.


```r
library(Entrez)
```

```
## Loading required package: RCurl
## Loading required package: bitops
## Loading required package: XML
```

```r
library(tm)
```

```
## Loading required package: NLP
```

```r
library(wordcloud)
```

```
## Loading required package: RColorBrewer
```

```r
# Download and parse records from PubMed
query <- '"new mexico"[AD] AND pharmacy[AD]'
records <- medlineParser(efetch(query, db="pubmed", type="medline"))
abstracts <- unlist(sapply(records, function(x) x["AB"][[1]]))
```

There were 535 records found.


```r
# Create corpus from abstracts
corpus <- VCorpus(VectorSource(abstracts))

# Remove white-space
corpus <- tm_map(corpus, stripWhitespace)

# Conver to lowercase
corpus <- tm_map(corpus, content_transformer(tolower))

# Remove punctuation
corpus <- tm_map(corpus, removePunctuation)

# Remove stopwords
corpus <- tm_map(corpus, function(x) removeWords(x, stopwords(kind="en")))

# Create Term-Document-Matrix
tdm <- as.matrix(TermDocumentMatrix(corpus))

# Get word frequencies by summing accross rows (n-dimension)
word_freqs <- rowSums(tdm)

# Sort words by decreasing freq
sorted_words <- sort(word_freqs, decreasing=TRUE)
sorted_words <- data.frame(word=names(sorted_words), freq=sorted_words)

# Choose color palate
color_palate <- brewer.pal(9, "Reds")

wordcloud(sorted_words$word, sorted_words$freq, scale=c(4, 0.1), 
          min.freq=2, max.words=200, random.order = T, colors = color_palate)
```

![](./README_files/figure-html/unnamed-chunk-2-1.png) 
